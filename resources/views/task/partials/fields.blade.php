<section>
    <header>
        <h2 class="text-lg font-medium text-gray-900 dark:text-gray-100">
            {{ __('Task Information') }}
        </h2>
    </header>

    <div class="mt-6 space-y-6">
        <div>
            <x-input-label for="name" :value="__('Title')" />
            <x-text-input id="title" name="title" type="text" class="mt-1 block w-full" :value="old('title', isset($task) ? $task->title : null)" required autofocus autocomplete="title" />
            <x-input-error class="mt-2" :messages="$errors->get('title')" />
        </div>

        <div>
            <x-input-label for="name" :value="__('Description')" />
            <x-text-area-input id="description" name="description" type="text" class="mt-1 block w-full" required>
                {{ isset($task) ? $task->description : null }}
            </x-text-area-input>
            <x-input-error class="mt-2" :messages="$errors->get('title')" />
        </div>

        <div>
            <x-input-label for="name" :value="__('Project')" />
            <select name="project_id" class="form-control w-full" required>
                <option></option>
                @foreach($projects as $project)
                    <option value="{{ $project->id }}" @if(isset($task) and $task->project_id == $project->id) selected @endif>{{ $project->name }}</option>
                @endforeach
            </select>
            <x-input-error class="mt-2" :messages="$errors->get('project_id')" />
        </div>

        <div>
            <x-input-label for="name" :value="__('Priority')" />
            <select name="priority_id" class="form-control w-full" required>
                <option></option>
                @foreach($priorities as $priority)
                    <option value="{{ $priority->id }}" @if(isset($task) and $task->priority_id == $priority->id) selected @endif>{{ $priority->name }}</option>
                @endforeach
            </select>
            <x-input-error class="mt-2" :messages="$errors->get('priority_id')" />
        </div>

        <div>
            <x-input-label for="name" :value="__('Board Column')" />
            <select name="board_column_id" class="form-control w-full" required>
                <option></option>
                @foreach($boardColumns as $boardColumn)
                    <option value="{{ $boardColumn->id }}" @if(isset($task) and $task->board_column_id == $boardColumn->id) selected @endif>{{ $boardColumn->name }}</option>
                @endforeach
            </select>
            <x-input-error class="mt-2" :messages="$errors->get('priority_id')" />
        </div>

        <div>
            <x-input-label for="name" :value="__('Assignee')" />
            <select name="user_assigned_id" class="form-control w-full" required>
                <option></option>
                @foreach($users as $item)
                    <option value="{{ $item->id }}" @if(isset($task) and $task->user_assigned_id == $item->id) selected @endif>{{ $item->name }}</option>
                @endforeach
            </select>
            <x-input-error class="mt-2" :messages="$errors->get('priority_id')" />
        </div>

        <div>
            <x-input-label for="name" :value="__('Reporter')" />
            <select name="user_reporter_id" class="form-control w-full" required>
                <option></option>
                @foreach($users as $item)
                    <option value="{{ $item->id }}" @if((isset($task) and $task->user_assigned_id == $item->id) or (!isset($task) and $user->id == $item->id)) selected @endif>{{ $item->name }}</option>
                @endforeach
            </select>
            <x-input-error class="mt-2" :messages="$errors->get('priority_id')" />
        </div>

        <div class="flex items-center gap-4">
            <x-primary-button>{{ __('Save') }}</x-primary-button>
        </div>
    </div>
</section>
