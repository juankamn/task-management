<?php

namespace Database\Seeders;

use App\Models\Project;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Project::firstOrCreate(['name' => 'Task Manager']);
        Project::firstOrCreate(['name' => 'Digital Clock']);
        Project::firstOrCreate(['name' => 'Calculator']);
    }
}
