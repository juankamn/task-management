<?php

namespace App\Http\Controllers;

use App\Models\BoardColumn;
use App\Models\Priority;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    public function create()
    {
        $priorities = Priority::getOrderBy('order', 'desc');
        $boardColumns = BoardColumn::getOrderBy();
        $projects = Project::getOrderBy();
        $users = User::getOrderBy();
        $user = Auth::user();

        return view('task.create', compact('priorities', 'boardColumns', 'projects', 'user', 'users'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:2',
            'description' => 'required|min:2',
            'project_id' => 'required',
            'priority_id' => 'required',
            'board_column_id' => 'required',
            'user_assigned_id' => 'required',
            'user_reporter_id' => 'required',
        ]);

        $input = $request->all();
        $task = Task::create($input);

        return redirect()->route('board')
            ->with('success', 'Task created successfully');
    }
}
