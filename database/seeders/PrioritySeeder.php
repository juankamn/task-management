<?php

namespace Database\Seeders;

use App\Models\Priority;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PrioritySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Priority::firstOrCreate(['name' => 'Lowest', 'order' => 1, 'logo' => '<img class="sc-19ime50-1 jPnJkx" src="https://level60.atlassian.net/images/icons/priorities/lowest.svg" width="16px" height="16px">']);
        Priority::firstOrCreate(['name' => 'Low', 'order' => 2, 'logo' => '<img class="sc-19ime50-1 jPnJkx" src="https://level60.atlassian.net/images/icons/priorities/low.svg" width="16px" height="16px">']);
        Priority::firstOrCreate(['name' => 'Medium', 'order' => 3, 'logo' => '<img class="sc-19ime50-1 jPnJkx" src="https://level60.atlassian.net/images/icons/priorities/medium.svg" width="16px" height="16px">']);
        Priority::firstOrCreate(['name' => 'High', 'order' => 4, 'logo' => '<img class="sc-19ime50-1 jPnJkx" src="https://level60.atlassian.net/images/icons/priorities/high.svg" width="16px" height="16px">']);
        Priority::firstOrCreate(['name' => 'Highest', 'order' => 5, 'logo' => '<img class="sc-19ime50-1 jPnJkx" src="https://level60.atlassian.net/images/icons/priorities/highest.svg" width="16px" height="16px">']);
    }
}
