<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'order',
        'logo',
    ];

    public static function getOrderBy($column = 'order', $order = 'asc')
    {
        return self::orderBy($column, $order)->get();
    }
}
