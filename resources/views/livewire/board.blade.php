<div>
    <div class="flex justify-end p-4 gap-6">
        <div class="items-center ml-6">
            <x-dropdown align="right" width="48">
                <x-slot name="trigger">
                    <button class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 dark:text-gray-400 bg-white dark:bg-gray-800 hover:text-gray-700 dark:hover:text-gray-300 focus:outline-none transition ease-in-out duration-150">
                        <div>@if($project == null) - Select Project - @else {{ $project['name'] }} @endif</div>

                        <div class="ml-1">
                            <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                            </svg>
                        </div>
                    </button>
                </x-slot>

                <x-slot name="content">
                        <x-dropdown-link wire:click="setProject(null)">
                            - Select Project -
                        </x-dropdown-link>
                    @foreach($projects as $project)
                        <x-dropdown-link wire:click="setProject({{$project}})">
                            {{ $project->name }}
                        </x-dropdown-link>
                    @endforeach
                </x-slot>
            </x-dropdown>
        </div>

        <div>
            <a href="{{ route('task.create') }}"><x-secondary-button>{{ __('Create Task') }}</x-secondary-button></a>
        </div>
    </div>

    <div class="flex gap-6 p-4">
        @foreach($boardColumns as $boardColumn)
            <div>
                <div class="w-96 scale-100 p-6 bg-white dark:bg-gray-800/50 dark:bg-gradient-to-bl from-gray-700/50 via-transparent dark:ring-1 dark:ring-inset dark:ring-white/5 rounded-lg shadow-2xl shadow-gray-500/20 dark:shadow-none transition-all duration-250 focus:outline focus:outline-2 focus:outline-red-500">
                    <h2 class="text-xl font-semibold text-gray-900 dark:text-white">{{ $boardColumn->name }}</h2>
                </div>

                <div id="boardColumn{{$boardColumn->id}}" class="w-96 scale-100 p-6 bg-white dark:bg-gray-800/50 dark:bg-gradient-to-bl from-gray-700/50 via-transparent dark:ring-1 dark:ring-inset dark:ring-white/5 rounded-lg shadow-2xl shadow-gray-500/20 dark:shadow-none transition-all duration-250 focus:outline focus:outline-2 focus:outline-red-500">

                    @foreach($tasks->where('board_column_id', $boardColumn->id) as $task)
                        <div id="task{{$task->id}}" class="mt-4 text-gray-500 dark:text-gray-400 text-sm leading-relaxed w-80 scale-100 p-6 bg-white dark:bg-gray-800/50 dark:bg-gradient-to-bl from-gray-700/50 via-transparent dark:ring-1 dark:ring-inset dark:ring-white/5 rounded-lg shadow-2xl shadow-gray-500/20 dark:shadow-none flex transition-all duration-250 focus:outline focus:outline-2 focus:outline-red-500 cursor-pointer" onclick='Livewire.emit("openModal", "edit-task", {{ json_encode(["task" => $task->id]) }})'>
                            <div>
                                <h2 class="text-xl font-semibold text-gray-900 dark:text-white">{{ $task->title }}</h2>

                                <p class="mt-4 text-gray-500 dark:text-gray-400 text-sm leading-relaxed">
                                    {{ $task->description }}
                                </p>
                                <p class="mt-4 text-gray-500 dark:text-gray-400 text-xs leading-relaxed">
                                    {{ $task->Priority->name }}
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>

    <script>
        @foreach($boardColumns as $boardColumn)
        new Sortable(boardColumn{{ $boardColumn->id }}, {
            group: 'shared', // set both lists to same group
            animation: 150,
            onEnd: function( event, ui ) {
                console.log(event.newIndex);
                @this.reOrder(event.item.id.replace('task', ''), event.to.id.replace('boardColumn', ''), event.newIndex);
            }
        });
        @endforeach
    </script>
</div>
