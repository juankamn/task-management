<?php

namespace App\Http\Livewire;

use App\Models\BoardColumn;
use App\Models\Priority;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use LivewireUI\Modal\ModalComponent;

class EditTask extends ModalComponent
{
    public $task;

    public $priorities;
    public $boardColumns;
    public $projects;
    public $users;
    public $user;

    // Attributes
    public $description;
    public $project_id;
    public $priority_id;
    public $board_column_id;
    public $user_assigned_id;
    public $user_reporter_id;
    public $title;

    public function mount(Task $task)
    {
        $this->priorities = Priority::getOrderBy('order', 'desc');
        $this->boardColumns = BoardColumn::getOrderBy();
        $this->projects = Project::getOrderBy();
        $this->users = User::getOrderBy();
        $this->user = Auth::user();
        $this->task = $task;

        $this->title = $task->title;
        $this->project_id = $task->project_id;
        $this->priority_id = $task->priority_id;
        $this->board_column_id = $task->board_column_id;
        $this->user_assigned_id = $task->user_assigned_id;
        $this->user_reporter_id = $task->user_reporter_id;
        $this->description = $task->description;
    }
    public function render()
    {
        return view('livewire.edit-task');
    }

    public function updateTask()
    {
        $data = [
            'title' => $this->title,
            'description' => $this->description,
            'project_id' => $this->project_id,
            'priority_id' => $this->priority_id,
            'board_column_id' => $this->board_column_id,
            'user_assigned_id' => $this->user_assigned_id,
            'user_reporter_id' => $this->user_reporter_id,
        ];
        $rules = [
            'title' => 'required|min:2',
            'description' => 'required|min:2',
            'project_id' => 'required',
            'priority_id' => 'required',
            'board_column_id' => 'required',
            'user_assigned_id' => 'required',
            'user_reporter_id' => 'required',
        ];
        $validator = Validator::make(
            $data,
            $rules
        );
        $validated = $validator->validate();
        $this->task->update($validated);

        $this->closeModal();
        $this->emit('sectionRefresh');
    }

    public static function modalMaxWidth(): string
    {
        return 'xl';
    }

    public function deleteTask()
    {
        $this->task->delete();

        $this->closeModal();
        $this->emit('sectionRefresh');
    }
}
