# Task Management

A laravel-based project.

## System requirements

- PHP 8.1
- MySQL 8.0
- Composer

## Development setup

There are many ways to set up Laravel projects to run locally. These instructions will get you started installing the required software locally on your system, assuming you are running on OS X and use Homebrew. If you are on Linx or Windows, a similar process should work for you, but you will have to substitute other package managers for `brew`. If you are on Windows, you can use the WSL or look at a virtualized solution like [Laravel Homestead](https://laravel.com/docs/10.x/homestead).

If you are running many different applications on your local machine and need different versions of PHP for each of them, you may want to use [asdf]() as a version manager to install them side-by-side and switch between them. There is a `.tool-versions` file in this project if you are using asdf. See the PHP plugin [install instructions](https://github.com/asdf-community/asdf-php) if you choose to use asdf.

To install PHP and MySQL:

```bash
# If using brew, if using asdf wait until you've cloned the project down and then `asdf install` from the project directory
brew install php@8.1
brew install mysql@8.0
# Check that mysql is configured to start
brew services list
# If you don't see mysql@8.0 running, start it
brew services start mysql@8.0
```

Install Composer following their [installation instructions](https://getcomposer.org/download/).

Now we can set up the application now that you've got your system requirements taken care of.

```bash
# Start in whatever directory you keep your projects
cd ~/projects
cd task-management

# Copy the example .env over to an actual .env file that will have an effect
cp .env.example .env

# Install dependencies
composer install

# Create an application key for your environment
php artisan key:generate

# Create the application database
mysql -u root -e 'create database task_management_dev;'

# Run the migrations to create tables
php artisan migrate

# Seed some test data into the database
php artisan db:seed

# It may be helpful to clear the cache and routes after making configuration changes like we've just done
php artisan cache:clear
php artisan route:clear

# Start up the application
php artisan serve
```

You should now be able to visit the application at http://localhost:8000 and log in using one of the logins in the [UserSeeder.php](./database/seeders/UserSeeder.php)
