<?php

namespace Database\Seeders;

use App\Models\BoardColumn;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BoardColumnSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        BoardColumn::firstOrCreate(['name' => 'Backlog', 'order' => 1]);
        BoardColumn::firstOrCreate(['name' => 'To Do', 'order' => 2]);
        BoardColumn::firstOrCreate(['name' => 'In Progress', 'order' => 3]);
        BoardColumn::firstOrCreate(['name' => 'QA', 'order' => 4]);
        BoardColumn::firstOrCreate(['name' => 'Done', 'order' => 5]);
    }
}
