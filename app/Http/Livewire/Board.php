<?php

namespace App\Http\Livewire;

use App\Models\BoardColumn;
use App\Models\Priority;
use App\Models\Project;
use App\Models\Task;
use Livewire\Component;

class Board extends Component
{
    public $boardColumns;
    public $projects;
    public $project;
    public $tasks;

    protected $listeners = ['sectionRefresh' => '$refresh'];

    public function mount()
    {
        $this->boardColumns = BoardColumn::getOrderBy();
        $this->projects = Project::getOrderBy();
    }

    public function render()
    {
        $this->getTasks();
        return view('livewire.board');
    }

    public function getTasks()
    {
        $this->tasks = Task::getOrderByPriority();
        if ($this->project != null) {
            $this->tasks = $this->tasks->where('project_id', $this->project['id']);
        }
    }

    public function setProject($project)
    {
        $this->project = $project;
    }

    public function reOrder(Task $task, BoardColumn $new_board_column, $new_index)
    {
        $priorities = Priority::getOrderBy('order', 'desc');
        $new_priority = $task->priority_id;
        foreach ($priorities as $key => $priority) {
            if ($key == $new_index) {
                $new_priority = $priority->id;
            }
        }
        $task->board_column_id = $new_board_column->id;
        $task->priority_id = $new_priority;
        $task->save();
    }
}
