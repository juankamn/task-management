<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'description',
        'priority_id',
        'board_column_id',
        'project_id',
        'user_assigned_id',
        'user_reporter_id',
    ];

    public function Priority()
    {
        return $this->belongsTo('App\Models\Priority');
    }

    public static function getOrderByPriority()
    {
        return self::select('tasks.*')->join('priorities', 'tasks.priority_id', '=', 'priorities.id')->orderBy('priorities.order', 'desc')->get();
    }
}
