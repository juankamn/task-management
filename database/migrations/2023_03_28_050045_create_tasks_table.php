<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('priority_id')->unsigned();
            $table->foreign('priority_id')->references('id')->on('priorities')->onDelete('cascade');
            $table->bigInteger('board_column_id')->unsigned();
            $table->foreign('board_column_id')->references('id')->on('board_columns')->onDelete('cascade');
            $table->bigInteger('project_id')->unsigned();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->bigInteger('user_assigned_id')->unsigned();
            $table->foreign('user_assigned_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('user_reporter_id')->unsigned();
            $table->foreign('user_reporter_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('title');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
};
