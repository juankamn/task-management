<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Task') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
                <div class="max-w-xl">
                    <form wire:submit.prevent="updateTask">
                        @csrf
                        <header>
                            <h2 class="text-lg font-medium text-gray-900 dark:text-gray-100">
                                {{ __('Task Information') }}
                            </h2>
                        </header>

                        <div class="mt-6 space-y-6">
                            <div>
                                <x-input-label for="name" :value="__('Title')" />
                                <x-text-input id="title" wire:model="title" type="text" class="mt-1 block w-full" required autofocus autocomplete="title" />
                                <x-input-error class="mt-2" :messages="$errors->get('title')" />
                            </div>

                            <div>
                                <x-input-label for="name" :value="__('Description')" />
                                <x-text-area-input id="description" wire:model="description" type="text" class="mt-1 block w-full" required></x-text-area-input>
                                <x-input-error class="mt-2" :messages="$errors->get('title')" />
                            </div>

                            <div>
                                <x-input-label for="name" :value="__('Project')" />
                                <select wire:model="project_id" class="form-control w-full" required>
                                    <option></option>
                                    @foreach($projects as $project)
                                        <option value="{{ $project->id }}">{{ $project->name }}</option>
                                    @endforeach
                                </select>
                                <x-input-error class="mt-2" :messages="$errors->get('project_id')" />
                            </div>

                            <div>
                                <x-input-label for="name" :value="__('Priority')" />
                                <select wire:model="priority_id" class="form-control w-full" required>
                                    <option></option>
                                    @foreach($priorities as $priority)
                                        <option value="{{ $priority->id }}">{{ $priority->name }}</option>
                                    @endforeach
                                </select>
                                <x-input-error class="mt-2" :messages="$errors->get('priority_id')" />
                            </div>

                            <div>
                                <x-input-label for="name" :value="__('Board Column')" />
                                <select wire:model="board_column_id" class="form-control w-full" required>
                                    <option></option>
                                    @foreach($boardColumns as $boardColumn)
                                        <option value="{{ $boardColumn->id }}">{{ $boardColumn->name }}</option>
                                    @endforeach
                                </select>
                                <x-input-error class="mt-2" :messages="$errors->get('priority_id')" />
                            </div>

                            <div>
                                <x-input-label for="name" :value="__('Assignee')" />
                                <select wire:model="user_assigned_id" class="form-control w-full" required>
                                    <option></option>
                                    @foreach($users as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                <x-input-error class="mt-2" :messages="$errors->get('priority_id')" />
                            </div>

                            <div>
                                <x-input-label for="name" :value="__('Reporter')" />
                                <select wire:model="user_reporter_id" class="form-control w-full" required>
                                    <option></option>
                                    @foreach($users as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                <x-input-error class="mt-2" :messages="$errors->get('priority_id')" />
                            </div>

                            <div class="flex justify-between gap-4">
                                <x-primary-button>{{ __('Save') }}</x-primary-button>
                                <x-danger-button wire:click='deleteTask()' onclick="return confirm('Are you sure you want to delete this task?') || event.stopImmediatePropagation()">Delete task</x-danger-button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
